import schedule
import time
from datetime import datetime, timedelta
import psycopg2  # You'll need to install the psycopg2 library for PostgreSQL

# Database connection parameters
db_params = {
    'host': '172.30.227.205',
    'database': 'sitcenter_postgis_datalake',
    'user': 'la_noche_estrellada',
    'password': 'Cfq,thNb13@',
    'port': '5439'
}

def update_status():
    try:
        # Connect to the PostgreSQL database
        conn = psycopg2.connect(**db_params)
        cursor = conn.cursor()

        # Your SQL update query
        sql_query = """
        UPDATE repair_works rw
        SET status_id = CASE
                            WHEN end_date < NOW() THEN 1
                            WHEN end_date <= current_date + interval '2 weeks' THEN 2
                            ELSE status_id
                        END
        WHERE rw.status_id IS NOT NULL
            AND rw.status_id NOT IN (1, 3, 7)
            AND end_date IS NOT NULL
            AND rw.sub_category_id NOT IN (18);
        """

        # Execute the SQL query
        cursor.execute(sql_query)
        conn.commit()
        
        print("Status updated successfully")

    except Exception as e:
        print(f"Error updating status: {str(e)}")
    finally:
        cursor.close()
        conn.close()

def timer():
    now = datetime.now()
    midnight = now.replace(hour=0, minute=5, second=0, microsecond=0)
    if now > midnight:
        midnight += timedelta(days=1)
    time_left = midnight - now

    hours_left, remainder = divmod(time_left.total_seconds(), 3600)
    minutes_left, _ = divmod(remainder, 60)
    
    print(f"Time left until update: {int(hours_left)} hours and {int(minutes_left)} minutes")

# Schedule the update to run every day at 00:05
schedule.every().day.at("00:05").do(update_status)

# Schedule the timer to run every minute
schedule.every(1).minutes.do(timer)  # Run every minute

while True:
    schedule.run_pending()
    time.sleep(1)
